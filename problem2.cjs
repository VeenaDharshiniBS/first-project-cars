//exporting
module.exports = function(data) {

    if(data === undefined || data.length==0 || Array.isArray(data)==false)
        return [];
    else
        return data[data.length-1];
}