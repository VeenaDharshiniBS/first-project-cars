//exporting
module.exports = function(cars) {
    
    let carModels = [];
    
    if(cars === undefined || cars.length==0 || Array.isArray(cars)==false)
        return carModels;

    for(let i=0;i<cars.length;i++)
        carModels.push(cars[i].car_model);

    carModels.sort(function (a, b) {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    
    return carModels;
}