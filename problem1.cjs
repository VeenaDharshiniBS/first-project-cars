function problem1(data,n)
{
    if(data === undefined || data.length===0 || Array.isArray(data)===false || Number.isNaN(n)===true || n===undefined)
        return [];  

    for(let i=0;i<data.length;i++)
    {
        if(data[i].id==n)
        {
            return data[i];
        }
    }
    
    return [];
}


module.exports = problem1;