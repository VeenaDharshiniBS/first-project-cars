//exporting
module.exports = function(cars,car1,car2) {
    
    let res =[];
    
    for (let i = 0; i < cars.length; i++) 
    {
        if (cars[i].car_make == car1 || cars[i].car_make == car2)
            res.push(cars[i]);
    }
    console.log(car1 + " and " + car2 + " array: ");
    console.log(res);
    
    console.log("\nIn JSON format: ");
    console.log(JSON.stringify(res));
    
}